package m.w.game.entities;

import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;

import m.w.game.gamestate.GameState;
import m.w.game.main.GamePanel;
import m.w.game.objects.Block;
import m.w.game.physics.Collision;

public class Player{

	private static final long serialVersionUID = 1L;
	
	private boolean right = false, left = false, jumping = false, falling = false;
	private boolean topCollision = false;
	
	private double x,y;
	private int width,height;
	
	private double maxFallSpeed = 10;
	private double currentFallSpeed = 1;
	
	private double jumpSpeed = 10;
	private double currentJumpSpeed = jumpSpeed;
	
	private double maxMoveSpeed = 7;
	private double currentMoveSpeed = 0;
	
	private boolean brejk = false;
	
	
	public Player(int width, int height){
		//x=GamePanel.WIDTH/2;
		//y=GamePanel.HEIGT/2;
		x=150;
		y=150;
		this.width = width;
		this.height = height;
	}
	public void tick(Block[] b){
		int iX = (int)x;
		int iY = (int)y;
		for(int i=0;i<b.length;i++){
			//prawo
			
			if(Collision.playerBlock(new Point(iX+width,iY+2),b[i])
					||Collision.playerBlock(new Point(iX+width, iY+height-1),b[i])){
					right = false;
			}
			
			//lewo
			if(Collision.playerBlock(new Point(iX-1,iY+2),b[i])
					||Collision.playerBlock(new Point(iX-1, iY+height-1),b[i])){
					left = false;
			}
			//od dolu
			if(Collision.playerBlock(new Point(iX+1,iY),b[i])
					||Collision.playerBlock(new Point(iX+width-1, iY),b[i])){		
				jumping = false;	
				falling = true;
			}
			//z gory
			if(Collision.playerBlock(new Point(iX+2,iY+height+1),b[i])
					||Collision.playerBlock(new Point(iX+width-1, iY+height+1),b[i])){
				y=b[i].getY() - height;
				falling = false;
				topCollision = true;
			}
			else{
				if(!topCollision && !jumping){
					falling=true;
				}
			}
		}
		
		topCollision = false;
		
		if(right){
			brejk=false;
			x+=currentMoveSpeed;
			if(currentMoveSpeed <= maxMoveSpeed){
				currentMoveSpeed+=0.1;
			}
		}
		
		if(left){
			brejk=false;
			x-=currentMoveSpeed;
			if(currentMoveSpeed <= maxMoveSpeed){
				currentMoveSpeed+=0.1;
			}
		}
		
		if(jumping){
		
			y-=currentJumpSpeed;
			currentJumpSpeed -= 0.5;
			if(currentJumpSpeed <= 0 ){
				currentJumpSpeed = jumpSpeed;
				jumping = false;
				falling=true;
			}
		}
		if(falling){
			//GameState.yOffSet+=currentFallSpeed;
			y+=currentFallSpeed;
			
			if(currentFallSpeed < maxFallSpeed){
				currentFallSpeed+=0.5;
			}
		}
		if(!falling){
			currentFallSpeed = 0.1;
		}
		if (brejk == true)currentMoveSpeed = 0;
	}
	
	public void draw(Graphics g){
		g.setColor(Color.BLACK);
		
		g.fillRect((int)x, (int)y, width, height);
	}
	public void keyPressed(int k){
		if(k==KeyEvent.VK_RIGHT)right=true;
		if(k==KeyEvent.VK_LEFT)left=true;
		if(k==KeyEvent.VK_UP && !jumping && !falling) jumping=true;
	//	if(k!=KeyEvent.VK_RIGHT)brejk = true;
	//	if(k!=KeyEvent.VK_LEFT)brejk = true;

	}
	public void keyReleased(int k){
		if(k==KeyEvent.VK_RIGHT){
			brejk = true;
			right=false;
		}
		if(k==KeyEvent.VK_LEFT){
			brejk = true;
			left=false;
		}
	}
}
