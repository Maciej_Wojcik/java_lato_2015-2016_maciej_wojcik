package m.w.game.objects;

import java.awt.Graphics;
import java.awt.Rectangle;

import m.w.game.gamestate.GameState;

public class Block extends Rectangle {
	
	public static final int blockSize = 35;
	public boolean killer = false;
	
	public Block(int x,int y){
		setBounds(x,y,blockSize,blockSize);
	}
	
	public void tick(){
	}
	
	public void draw(Graphics g){
		//int xpoints[] = {x+25, x+145, x+25, x+145, x+25};
	    //int ypoints[] = {y+25, y+25, y+145, y+145, y+25};
	    //int npoints = 5;
	    
	    //g.drawPolygon(xpoints, ypoints, npoints);
		//g.fillOval(x, y, blockSize, blockSize);
		g.fillRect(x, y , width, height);
	}
	
}
