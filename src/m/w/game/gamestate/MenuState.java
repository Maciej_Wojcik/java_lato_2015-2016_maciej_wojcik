package m.w.game.gamestate;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import m.w.game.main.GamePanel;

public class MenuState extends GameState {

	private String[] options = {"Start",
							    "Options",
							    "Exit"
	};
	private int currentSelection = 0;
	
	public MenuState(GameStateManager gsm) {
		super(gsm);
		
	}

	
	protected void init() {}

	protected void tick() {
		
	}

	
	protected void draw(Graphics g) {
		
		g.setColor(new Color(0,0,0));
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGT);
		
		for(int i=0;i<options.length;i++){
			if(i == currentSelection){
				g.setColor(Color.YELLOW);
			}
			else{
				g.setColor(Color.GRAY);
			}
			g.setFont(new Font("Arial",Font.PLAIN,120));
			g.drawString(options[i],GamePanel.WIDTH/2-100,100+i*200);
		}
	}

	
	protected void keyPressed(int k) {
	
		if(k==KeyEvent.VK_DOWN){
			currentSelection++;
			if(currentSelection>=options.length){
				currentSelection=0;
			}
			
		}else if(k==KeyEvent.VK_UP){
			currentSelection--;
			if(currentSelection<0){
				currentSelection = options.length-1;
			}
		}
		if(k==KeyEvent.VK_ENTER){
			if(currentSelection==0){
				gsm.states.push(new LevelState(gsm));
			}else if(currentSelection==1){
				
			}else if(currentSelection==2){
				System.exit(0);
			}
		}
		
	}

	
	protected void keyReleased(int k) {
	
		
	}

}
