package m.w.game.gamestate;

import java.awt.Graphics;

import m.w.game.entities.Player;
import m.w.game.objects.Block;

public class LevelState extends GameState{

	private Player player;
	
	private Block[] b;
	
	public LevelState(GameStateManager gsm) {
		super(gsm);
		
	}

	
	public void init() {
		player = new Player(20,30);
		b=new Block[60];
		//b[0] = new Block(150,300);
		//b[1] = new Block(220,250);
		//b[2] = new Block(300,300);
		for(int i=0;i<40;i++){
			b[i] = new Block(i*10,800);
		}
		for(int i=40;i<60;i++){
			b[i] = new Block((i-10)*11,700);
		}
	}


	public void tick() {
		
		for(int i=0;i<b.length;i++){
			b[i].tick();
		}
		
		player.tick(b);
		
	}

	
	public void draw(Graphics g) {
		player.draw(g);
		for(int i=0;i<b.length;i++){
			b[i].draw(g);
		}
	}

	public void keyPressed(int k) {
		
		player.keyPressed(k);
	}

	public void keyReleased(int k) {
		player.keyReleased(k);
	}

}
