package m.w.game.gamestate;

import java.awt.Graphics;

public abstract class GameState {

	protected GameStateManager gsm;
	public static double xOffSet,yOffSet;
	
	public GameState(GameStateManager gsm){
		this.gsm=gsm;
		
		this.xOffSet=0;
		this.yOffSet=0;
		
		init();
	}
		protected abstract void init();
		protected abstract void tick();
		protected abstract void draw(Graphics g);
		protected abstract void keyPressed(int k);
		protected abstract void keyReleased(int k);
}
